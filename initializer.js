(function (_) {

	var inquirer = require('inquirer');
	var executor = require('./executor.js');
	var logger = require("./util/logger");
	var glob = require("glob")
	var Promise = require("promise");


	var folder = "./services";
	var relative = "./../../";

	_.setFolder = function (newFolder) {
		folder = newFolder;
	}

	/**
     * initialize is in ./lib/util. It must have the relative path to your endpoint be seen
     */
	_.setRelativePath = function (newRelative) {
		relative = newRelative;
	}


	_.init = function () {

		glob(folder + "/*", null, function (er, files) {
			inquirer.prompt([
				{
					type: 'list',
					name: 'domain',
					message: 'Choose Domain',
					choices: files
				}
			]).then(function (answers) {

				glob(answers.domain + "/*.js", null, function (er, files) {


					inquirer.prompt([
						{
							type: 'list',
							name: 'endpoint',
							message: 'Choose Endpoint',
							choices: files
						}
					]).then(function (answers) {
						path = relative + answers.endpoint;
						try {
							endpoint = require(path);
							executor.setEndPoint(endpoint);
							executor.multipleExecution();
						} catch (exc) {
							var error = require("./util/errorholder")();
							error.setError("Problem while starting endpoint.", exc);
							error.log();
						}

					});

				})

			});

		});
	}

	return _;

})(module.exports);