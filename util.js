(function(_){
	var logger = require("./util/logger");

	_.wait = function(ms) {
		var start = new Date().getTime();
		var end = start;
		while (end < start + ms) {
			end = new Date().getTime();
		}
	}


	_.waitPromise = function(ms) {
		var p = new Promise(function(resolve, reject) {
			var ref = setTimeout(function() {
				clearTimeout(ref);
				resolve();
			}, ms);
		});
		return p;
	}

	_.isJSON = function (text){
		try{
			JSON.parse(text);
			return true;
		}
		catch (error){
			return false;
		}
	};
	_.toJSON = function(text) { 
		try{
			return JSON.parse(text);
		}
		catch (error){
			logger.throwError("Error while parsing json:" + text + " - error: " + error);
		}

	}

})(module.exports);