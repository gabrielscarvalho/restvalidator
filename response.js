function Response() {

	var PromiseTimeOut = require("promisetimeout");
	var Promise = require("promise");
	var duration = require("./util/duration");
	var util = require("./util");
	var logger = require("./util/logger");

	_ = {};

	_.config = {
		/* Change this attribute if your checkResponse will take more time than it.*/
		checkResponseTimeOutMs: 5000
	}

	_.statusCode = null;
	_.body = {};
	_.duration = duration;
	_.headers = {};
	_.isValid = null;
	_.error = null;

	var private = {
		checkResponse: null
	};

	_.reset = function () {
		this.statusCode = null;
		this.body = {};
		this.duration = duration;
		this.headers = {};
		this.isValid = null;
		this.error = null;
		return this;
	}

	_.setCheckResponseTimeOut = function (ms) {
		_.config.checkResponseTimeOutMs = ms;
		return this;
	}
	_.loadResponse = function (response, body) {
		this.reset();
		this.statusCode = response.statusCode;
		this.headers = response.headers;

		if (typeof body == 'string' && util.isJSON(body)) {
			body = util.toJSON(body);
		}

		this.body = body;
		return this;
	}

	_.checkResponse = function (func, timeOut) {
		private.checkResponse = func;
		
		if(timeOut){
			logger.debug("Changing Response.checkResponse timeout from: ["+ this.config.checkResponseTimeOutMs+ "]ms to: [" + timeOut + "]ms");
			this.config.checkResponseTimeOutMs = timeOut;
		}
	}
	_.hasCheckResponse = function () {
		return typeof private.checkResponse === "function";
	}


	_.check = function (request) {
		logger.debug("Executing asserts...");
		var response = this;

		var p = new PromiseTimeOut(function (holder) {
			response.isValid = true;

			if (!response.hasCheckResponse()) {
				logger.debug("\tThere is no function to validate calls. Marked as success.")
				holder.resolve({ hasNotCheckResponse: true, message: 'Check response not found.' });
			}
			try {
				private.checkResponse(request, response, holder);
				//holder.resolve() must be called from checkResponse.

			} catch (exc) {
				response.isValid = false;
				if (exc == null || !exc.isErrorHolder) {
					logger.debug("\tYour response.checkResponse method has throwed an exception that is not instance of 'rest-client-tester/util/errorholder'. It will be marked as unknown error. Have you tried to use 'rest-client-tester/util/assert' ?");
					exc = require("./util/errorholder")().setError("Unknown problem while checking response", exc);
				}
				holder.reject(exc);
			}
		}, this.config.checkResponseTimeOutMs, "Have you put promise.resolve() in your checkResponse method? Or do you need more time to execute this assert? In this case use Response.setCheckResponseTimeOut(ms).");

		return p;
	}

	_.toString = function () {
		return " time: [" + this.duration.toMs() + "ms] statusCode: [" + this.statusCode + "]";
	}

	return _;

}

module.exports = Response;
