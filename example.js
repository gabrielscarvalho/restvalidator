var initialize = require("./initializer");
require("./util/logger").isDebugActive = false;
require("./util/logger").showDebugRequest = false;

initialize.setRelativePath("");
initialize.setFolder("./examples");
initialize.init();
