(function (_) {

	var started;
	var ended;

	_.start = function () {
		started = new Date();
	}

	_.end = function () {
		ended = new Date();
	}

	_.toMs = function () {
		return (ended - started) / 10;
	}

	_.getWhenStarted = function () {
		return started;
	}

	_.getWhenEnded = function () {
		return ended;
	}

	return _;
})(module.exports);