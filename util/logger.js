(function (_) {
	var colors = require('colors');
	var util = require("util");

	_.isDebugActive = false;
	_.showDebugRequest = false;
	var reference = null;

	function prepareMessage(message) {
		data = Date().toString().slice(16, 24);
		return "[" + data + "] - " + message;
	}

	_.header = function (message) {
		console.log(prepareMessage(message.bold));
	}

	_.warn = function (message) {
		console.log(prepareMessage(message.yellow));
	}


	_.warnBold = function (message) {
		console.log(prepareMessage(message.yellow.bold));
	}


	_.success = function (message) {
		console.log(prepareMessage(message.green));
	}


	_.info = function (message) {
		console.log(prepareMessage(message.blue.bgWhite));
	}
	_.setReference = function (newReference) {
		reference = "[" + newReference + "] ";
	}


	_.debug = function (message) {
		message = "[DEBUG] " + reference + "- " + message;
		if (this.isDebugActive) {
			console.log(prepareMessage(message.blue));
		}
	}

	_.debugRequest = function (message, value) {
		if (this.isDebugActive && this.showDebugRequest) {
			this.debug(message);
			try {
				this.debug(JSON.stringify(value));
			} catch (exc) {
				this.debug(value);
			}
		}
	}


	_.errorBold = function (message) {
		console.log(prepareMessage(message.red.bold));
	}

	_.error = function (message) {
		console.log(prepareMessage(message.red.bgWhite));
	}
	
	_.throwError = function (message) {
		_.error(message);
		process.exit();
	}

})(module.exports);