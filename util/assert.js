(function(_){
	

	_.isItOk = true;

	_.type = {
		object : 'object',
		string : 'string',
		func : 'function',
		undef : 'undefined'
	}

	_.throwExc = function(message){
		var error = require("./errorholder")();
		error.setError("Failed when executing asserts.",  message || "Assert is not valid!");
		throw error;
	}


	_.assertTrue = function(variable, message) {
		if(!variable){
			_.throwExc(message);
		}
	}

	_.assertFalse = function(variable, message) {
		_.assertTrue(!variable, message);
	}

	_.assertEquals = function(expected, received, message) { 
		if(expected !== received){
			_.throwExc(message + " - expected: [" + expected + "] - received: [" + received + "] .");
		}
	}

	_.assertNotEquals = function(expected, received, message){
		if(expected === received){
			_.throwExc(message + " - expected: [" + expected + "] - received: [" + received + "] .");
		}
	}

	_.assertNull = function(variable, message) {
		if(typeof variable !== "undefined" && variable !== null && variable !== ''){
			_.throwExc(message + " - received: [" + variable + "]");
		}
	}

	_.assertNotNull = function(variable, message) {
		if(typeof variable === "undefined" || variable === null || variable === ''){
			_.throwExc(message);
		}
	}

	_.assertType = function(variable, type, message){
		if(typeof variable !== type) {
			_.throwExc(message+ " - received: [" + variable + "]");	
		}
	}

})(module.exports);