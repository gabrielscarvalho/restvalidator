
function ErrorHolder() {

    var logger = require("./logger");

    _ = {};
    _.message = null;
    _.error = null;
    _.stack = null;
    _.extraStack = null;
    _.ref = null;
    _.isErrorHolder = true;
    _.previousError = null;

    _.setRef = function (ref) {
        this.ref = ref;
        return this;
    }

    _.load = function(message, error) {
        if(error == null) {
            error = {};
        }

        if(error.isErrorHolder) {
            error.message = message + " : "+ error.message;
            return error;
        }

        return this.setError(message, error);
    }

    _.setError = function (message, error) {
        this.message = message;
        if (error == null) {
            error = {};
        }

        if (error.isErrorHolder) {
            this.previousError = error;
            this.error = error.error;
        } else {
            this.error = error;
        }
        if (error.stack) {
            this.setStack(error.stack);
        } else{
            logger.debug("This error: ["+ error +"] has no stack. Check if you throwed a string instead of throw new Error(msg) ")
        }

        var x = new Error();
        this.stack = x.stack;

        return this;
    }

    _.setStack = function (stack) {
        this.extraStack = stack;
        return this;
    }

    _.logAndStopProcess = function () {
        _.log();
        logger.errorBold("STOPPED PROCESS.");
        process.exit();
    }

    _.getRootError = function () {
        if (this.previousError != null) {
            return this.previousError.getFirstError();
        }
        return this;
    }


    _.log = function () {
        logger.errorBold(this.message);
        if (typeof this.error == 'object') {
            console.log(this.error);
        } else {
            logger.error(this.error.toString());
        }

        if (this.extraStack != null) {
            logger.warnBold("Place that throwed the error:");
            logger.warn(this.extraStack);
        }
        logger.warnBold("Place that captured the error:");
        logger.warn(this.stack);

        if (this.previousError != null) {
            logger.warnBold("Previous Error:");
            _.previousError.log();
        }
    }


    return _;
}
module.exports = ErrorHolder;