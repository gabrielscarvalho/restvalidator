(function (_) {

	var logger = require("./util/logger.js");
	var Promise = require("promise");
	var util = require("./util.js");

	_.endpoint = null;

	_.setEndPoint = function (endpoint) {
		if (typeof endpoint !== 'function') {
			throw new Error("Endpoint must be a function!")
		}
		_.endpoint = endpoint();
		if (!_.endpoint.isEndPoint) {
			throw new Error("Endpoint must be a instance of Endpoint!");
		}
	}

	_.multipleExecution = function () {
		logger.setReference(_.endpoint.retry.getRetries());
		logger.debug("------------STARTING-------------");
		try {
			_.endpoint.doRequest().then(
				function (response) {
					executeAsserts();
				},
				function (err) {
					_.endpoint.fail(err);
					tryToRetry();
				}
			).catch(function (err) {
				var error = require("./util/errorholder")();
				error.setError("Some error happened while executing request", err);
				error.logAndStopProcess();
			});
		} catch (exc) {
			var error = require("./util/errorholder")();
			error.setError("Unknown error while multiple execution.", exc);
			error.logAndStopProcess();

		}
	}



	/** Execute asserts from the endpoint and check if must continue execution.*/
	function executeAsserts() {
		_.endpoint.executeAsserts().then(
			function () {
				_.endpoint.success();
				logger.debug("\tExecuted asserts - printing response:");
				tryToRetry();

			}, function (err) {

				logger.debug("\tFailed asserts - printing response");
				var message = "";
				if (err) {
					var error;
					if (err.timeOut) {
						error = require("./util/errorholder")().setError("Timeout while executing asserts.", err.message);
					} else if (err.isErrorHolder) {
						error = err;
					} else {
						error = require("./util/errorholder")().setError("Unknown error.", err);
					}
				}
				_.endpoint.fail(error);
				tryToRetry();
			}
		);
	}

	function tryToRetry() {
		_.endpoint.collectStatistics();
		_.endpoint.retry.increase();
		logger.debug("Checking if must retry.");
		if (_.endpoint.retry.isActive()) {
			logger.debug("\tRetrying.");
			logger.debug("------------FINISHED-------------\n")
			util.wait(_.endpoint.retry.getTimeInMs());
			_.multipleExecution();
		} else {
			logger.debug("------------FINISHED-------------\n")
		}

	}



	return _;
})(module.exports);