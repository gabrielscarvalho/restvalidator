(function(_){

	var logger = require("./util/logger");
	
	var private = {
		isActive : false,
		eachTime: 10000,
		retries: 0
	}



	_.retryEach = function(timeMs) {
		private.isActive = true;
		private.eachTime = timeMs;
		return this;
	}

	_.setIsActive = function(isActive) {
		if(!isActive){
			logger.debug("\tInactivating retries.");
		}
		private.isActive = isActive;
	}
	
	_.getTimeInMs = function(){
		return private.eachTime;
	}
	_.isActive = function(){
		return private.isActive;
	}

	_.disable = function() {
		private.isActive = false;
		return this;
	}

	_.increase = function() {
		private.retries++;
		return this;
	}

	_.getRetries = function(){
		return private.retries;
	}
	_.toString = function(){
		return "[#" + _.getRetries()+"]";
	}
	return _;
})(module.exports);



