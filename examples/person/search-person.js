function Endpoint(){
	
	var endpoint = require("../../endpoint.js")();
	var assert = require("../../util/assert");
	var logger = require("../../util/logger");



	endpoint.retry.retryEach(500);
	

	var id = 2;

	endpoint
		.request
			.setUrl("http://demo3840477.mockable.io/person/search/id/")
			.setUrlSuffix(id)
			.setRequestType("GET")
			.setTimeOut(4000);
			//.addHeader("Authorization", "Basic c4...");
			
	endpoint.request
		.updateRequest(function(request, response, promise) {
            logger.debug("Updating search-person request");

			if(endpoint.retry.getRetries() > 0) {
			    id++;
            }
			request.setUrlSuffix(id);
			promise.resolve(request);
		}
	);

	endpoint.response
		.checkResponse(function(request, response, promise) {
			logger.debug("Executing asserts from search-person");
			assert.assertEquals(200, response.statusCode, "Status code is unespected.");
			assert.assertType(response.body, 'object' , 'Response should be an object');
			assert.assertEquals(parseInt(response.body.id), request.urlSuffix, "Should return the id that was send.");
			promise.resolve();
		}
	);

	endpoint.setId = function(idPerson) {
		this.request.setUrlSuffix(idPerson);
		this.request.config.updateNextRequest = false;
		return this;
	}
	
	endpoint.collectStatistics = function(){
		/*
		examples: 
		endpoint.response.isValid;
		endpoint.response.errorMessage;
		endpoint.response.statusCode;
		endpoint.request.urlSuffix;
		endpoint.request.body;
		endpoint.response.duration.getMs();
		endpoint.response.duration.getWhenStarted();
		endpoint.retry.getRetries()
		ajax.post(data);
		file.write(data);
		mongodb.save(data);
		*/

	}	
	
	return endpoint;

}

module.exports = Endpoint;