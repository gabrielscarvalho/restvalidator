function Endpoint(){
	
	var endpoint = require("../../endpoint.js")();
	var assert = require("../../util/assert");
	var logger = require("../../util/logger");


	endpoint.retry.retryEach(500);
	

	endpoint
		.request
			.setUrl("https://demo3840477.mockable.io/order/last")
			.setRequestType("POST")
			.setTimeOut(4000)
			.setBody({ when: '10-10-2010' });
			
    //This will stop ONLY next request to execute update.
    //The second call will execute updateRequest.            
    endpoint.request.config.updateNextRequest = false;

	endpoint.request
		.updateRequest(function(request, response, promise) {
            request.setBody({ when: '11-11-2011'}); 
			promise.resolve(request);
		}
	);

	endpoint.response
		.checkResponse(function(request, response, promise) {
			logger.debug("Executing asserts from search-person");
			assert.assertEquals(200, response.statusCode, "Status code is unespected.");
			assert.assertType(response.body, 'object' , 'Response should be an object');
			assert.assertEquals(parseInt(response.body.id), 3004, "Should return id 3004.");
			promise.resolve();
		}
	);

		
	return endpoint;

}

module.exports = Endpoint;