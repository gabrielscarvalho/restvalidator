function Request(){

	var _ = {};
	var logger = require("./util/logger");
	var PromiseTimeOut = require("promisetimeout");

	_.url = null;
	_.urlSuffix = "";
	_.type = "GET";
	_.headers = {};
	_.body = null;
	_.timeOut = 10000;

	_.config = {
		/* Change this attribute if your checkResponse will take more time than it.*/
		updateRequestTimeOutMs : 5000,
		/*Must update the next call? if dont, will skip, and set this flag to true.*/
		updateNextRequest : true	
	}



	var private = {
		updateRequest : null
	};

	_.setUpdateRequestTimeOut = function(ms){
		_.config.updateRequestTimeOutMs = ms;
		return this;
	}

	_.setUrl = function(url){
		this.url = url;
		return this;
	}

	_.setUrlSuffix = function(urlSuffix){
		this.urlSuffix = urlSuffix;
		return this;
	}

	_.setRequestType = function(type){
		this.type = type;
		return this;
	}

	_.setTimeOut = function(timeOut){
		this.timeOut = timeOut;
		return this;
	}

	_.setBody = function(body) {
		this.body = body;
		return this;
	}

	_.addHeader = function(name, value){
		this.headers[name] = value;
		return this;
	}

	_.getHeader = function(name) {
		return this.headers[name];
	}

	_.asJson = function(){
		var isJson = this.type.toUpperCase() !== 'GET';
		return {
			method: this.type,
			timeout: this.timeOut,
			json: isJson,
			url: this.url + this.urlSuffix,
			headers : this.headers,
			body: this.body
		};

	}

	_.updateRequest = function(func, timeOut){
		private.updateRequest = func;
		if(timeOut){
			logger.debug("Changing Response.updateRequest timeout from: ["+ this.config.updateRequestTimeOutMs+ "]ms to: [" + timeOut + "]ms");
			this.config.updateRequestTimeOutMs = timeOut;
		}
	}

	_.update = function(response) {
		logger.debug("Updating request.");
		var request = this;
		var p = new PromiseTimeOut(function(holder) {

			if(!request.config.updateNextRequest) {
				logger.debug("\tStopped updating this request, due to request.config.updateNextRequest flag as false.");
				request.config.updateNextRequest = true;
				logger.debug("\tThe flag request.config.updateNextRequest is now true for next request.");
				holder.resolve(request);
			} else {

				if(typeof private.updateRequest !== 'function') {
					logger.debug('\tThere is no updateRequest function. Using the same object as request.');
					holder.resolve(request);
				}
				try{
					private.updateRequest(request, response, holder);
				} catch(exc) {
					var errorHolder = require("./util/errorholder")();
					errorHolder.setError("Unespected error while updating request - check if your request.checkResponse.", exc);
					errorHolder.log();
					holder.reject(errorHolder);
				}
			}
		}, _.config.updateRequestTimeOutMs, "Have you forgot to put promise.resolve() in your update method? Or do you need more time executing update? Use request.setUpdateRequestTimeOut(ms).");
		return p;
	}


	_.toString = function(){
		return "[" + this.type + "] " + this.url + this.urlSuffix;
	}

	_.isRequestObject = "yes";
	return _;

}
module.exports = Request;