function EndPoint() {

	var request = require("./request")();
	var response = require("./response")();
	var requester = require("request");
	var logger = require("./util/logger");
	var Promise = require("promise");

	var retry = require("./retry");

	var _ = {};


	_.request = request;
	_.response = response;
	_.retry = retry;
	_.event = {
		stopWhen: {
			hasSucceed: false,
			hasFailed: false
		}

	};
	_.isEndPoint = true;

	_.doRequestAndAsserts = function () {
		var holder = require("./util/errorholder");
		var self = this;

		logger.debug("Executing request & asserts...");

		var p = new Promise(function (resolve, reject) {
			self.doRequest().then(function () {
				logger.debug("\tRequest returned ok. Executing asserts...");

				self.executeAsserts().then(function () {
					logger.debug("\t\Asserts returned ok!");
					resolve();
				}, function (err) {
					reject(holder.setError("Error while asserting request", err));
				}).catch(function (err) {
					reject(holder.setError("Uncaught error while asserting request", err));
				});

			}, function (err) {
				reject(holder.setError("Error while executing request", err));
			}).catch(function (err) {
				reject(holder.setError("Uncaught error while executing request", err));
			});
		});
		return p;
	}

	_.doRequest = function () {
		logger.debug("Initializing request.");
		var response = this.retry.getRetries() == 0 ? null : this.response;

		var p = new Promise(function (resolve, reject) {

			_.request.update(response)
				.then(
				function (request) {
					if (typeof request !== 'object' || request.isRequestObject == null) {
						logger.throwError("UpdateRequest function must return promise.resolve(request) - promise is the 3rd arg.");
					}
					_.request = request;
					logger.debug("\tUpdated request step done.");
					_doRequest(resolve, reject);

				}, function (err) {
					var error = require("./util/errorholder")().setError("Rejected update request.", err);
					reject(error);
				}
				).catch(function (exc) {
					var error = require("./util/errorholder")();
					error.setError("Problem while updating request.", exc);
					error.logAndStopProcess();
				});
		});
		return p;
	}

	_.toString = function () {
		return _.retry.toString() + " " + _.request.toString() + " " + _.response.toString();
	}

	_.collectStatistics = function () {
		logger.debug("There is no method for collecting stats. Create the method: endpoint.collectStatistics and collect as you wish. Tip: use endpoint.response.isValid attribute!")
		return;
	}

	_.success = function () {
		this.response.isValid = true;
		this.retry.setIsActive(!this.event.stopWhen.hasSucceed);
		if (this.event.stopWhen.hasSucceed) {
			logger.debug("Successful! Stopping due to: endpoint.event.stopWhen.hasSucceed flag.");
		}
		logger.header(this.toString());
	}
	_.isRequestNumber = function (number) {
		return this.retry.getRetries() == number;
	}

	_.fail = function (error) {
		this.retry.setIsActive(!this.event.stopWhen.hasFailed);
		this.response.isValid = false;
		this.response.error = error;

		if (this.event.stopWhen.hasFailed) {
			logger.debug("Failed! Stopping due to: endpoint.event.stopWhen.hasFailed flag.");
		}

		logger.errorBold(this.toString());
		logger.warn(this.retry.toString() + " - " + error.message + " : " + error.error);
	}

	_.executeAsserts = function () {
		logger.debug("Asserting for request: " + this.toString());
		return this.response.check(this.request);
	}

	function _doRequest(resolve, reject) {
		var service = _.request.asJson();
		var response = _.response.reset();

		response.duration.start();

		logger.debug("Executing request:");
		logger.debugRequest("Request:", service);
		requester(service, function (err, res, body) {

			response.duration.end();

			if (err) {
				logger.debug("\tExecuted request with error." + err);
				var error = require("./util/errorholder")().setError("Error while executing request", err);
				reject(error);

			} else {
				logger.debug("\tExecuted successful request.");
				logger.debugRequest("Response", body);
				_.response = response.loadResponse(res, body);
				resolve(_.response);
			}
		});
	}

	return _;
}

module.exports = EndPoint;